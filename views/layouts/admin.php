<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\components\AuthWidget;

AppAsset::register($this);
$this->registerCssFile('css/dashboard.css', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::$app->name ?> - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
        'innerContainerOptions' => [
        	'class' => 'container-fluid'
        ]
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная админ.', 'url' => ['/admin/index']],
            ['label' => 'Главная сайта', 'url' => ['/site/index']],
            ['label' => 'Контакты', 'url' => ['/site/contact']],
        ],
    ]);
    NavBar::end();
    ?>

<div class="container-fluid">
    <div class="row">
    	<div class="col-sm-3 col-md-2 sidebar">
    	<?= Nav::widget([
	        'options' => ['class' => 'nav-sidebar'],
	        'items' => [
	            ['label' => 'Пользователи', 'url' => ['/admin/users/index']],
	            ['label' => 'Заявки', 'url' => ['/admin/claims/index']],
	            ['label' => 'Новости', 'url' => ['/admin/news/index']],
	        ],
	    ]); ?>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <?= $content ?>
        </div>
	</div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
