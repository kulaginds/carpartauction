<?php

/* @var $this yii\web\View */

$this->title = 'Страница редактирования пользователя';
$this->registerJsFile('js/jquery.validate.min.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/jquery.validate.additional.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/user-edit.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
?>
          <h1><?= $this->title ?></h1>
          <form class="form-horizontal" id="user_edit_form" method="post">
            <div class="form-group">
              <label for="inputName1" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-10">
                <input name="name" type="text" class="form-control" id="inputName1" placeholder="Name">
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail2" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <input name="email" type="email" class="form-control" id="inputEmail2" placeholder="Email">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-10">
                <input name="password" type="text" class="form-control" id="inputPassword3" placeholder="Password">
                <a href="#" class="btn btn-primary" onclick=" return generateTo('#inputPassword3')">Generate</a>
              </div>
            </div>
            <div class="form-group">
              <label for="inputType5" class="col-sm-2 control-label">User type</label>
              <div class="col-sm-10">
                <select name="type" id="inputType5" class="form-control">
                  <option value="1">поставщик</option>
                  <option value="2">поисковик</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputAddress6" class="col-sm-2 control-label">Address</label>
              <div class="col-sm-10">
                <input name="address" type="text" class="form-control" id="inputAddress6" placeholder="Address">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Save</button>
              </div>
            </div>
          </form>