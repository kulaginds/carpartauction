<?php

/* @var $this yii\web\View */

$this->title = 'Детальная страница заявки';
?>
          <h1><?= $this->title ?></h1>
          <div class="panel panel-info">
            <div class="panel-body">
              <h4>Search</h4>
              <form class="form-inline">
                <div class="form-group">
                  <label for="model">Car model</label>
                  <select name="model" id="model" class="form-control">
                    <option value="1">Mazda</option>
                    <option value="2">Chevrolet</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="part">Car part</label>
                  <input type="text" class="form-control" id="part" placeholder="window">
                </div>
                <button type="submit" class="btn btn-default">Go</button>
              </form>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Model</th>
                  <th>Part</th>
                  <th>User</th>
                  <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>Mazda</td>
                  <td>window</td>
                  <td><a href="#">User name</a></td>
                  <td>
                    <div class="btn-group" role="group">
                      <a href="#" class="btn btn-info">Edit</a>
                      <a href="#" class="btn btn-danger">Delete</a>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <nav>
            <ul class="pagination">
              <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>