<?php

/* @var $this yii\web\View */

$this->title = 'Страница создания новости';
$this->registerJsFile('js/news-edit.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
?>
          <h1><?= $this->title ?></h1>
          <div class="panel panel-default">
            <div class="panel-body">
              <form class="form-horizontal" id="proposition_form" method="post">
                <div class="form-group">
                  <label for="inputTitle1" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-10">
                    <input name="title" type="text" maxlength="40" class="form-control" id="inputTitle1" placeholder="Title">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputDate2" class="col-sm-2 control-label">Time</label>
                  <div class="col-sm-10">
                    <input name="time" type="datetime-local" id="news-time" class="form-control" id="inputDate2">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputText3" class="col-sm-2 control-label">Text</label>
                  <div class="col-sm-10">
                    <textarea name="text" maxlength="255" id="inputText3" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Create</button>
                  </div>
                </div>
              </form>
            </div>
          </div>