<?php

/* @var $this yii\web\View */

$this->title = 'Детальная страница заявки';
$this->registerJsFile('js/jquery.validate.min.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/jquery.validate.additional.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/proposition.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
?>
          <h1><?= $this->title ?></h1>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Car model: car part name</h3>
            </div>
            <div class="panel-body">
              <small>Published: <time datetime="">datetime</time></small>
            </div>
          </div>
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">Add proposition <small>для поставщиков</small></h3>
            </div>
            <div class="panel-body">
              <form class="form-horizontal" id="proposition_form" method="post">
                <div class="form-group">
                  <label for="inputCost1" class="col-sm-2 control-label">Part cost</label>
                  <div class="col-sm-10">
                    <input name="cost" type="number" class="form-control" id="inputCost1" placeholder="Part cost">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress2" class="col-sm-2 control-label">Stock address</label>
                  <div class="col-sm-10">
                    <input name="address" type="text" class="form-control" id="inputAddress2" placeholder="Stock address">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputComment3" class="col-sm-2 control-label">Comment</label>
                  <div class="col-sm-10">
                    <textarea name="comment" maxlength="255" id="inputComment3" cols="30" rows="10" class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Send</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="list">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">User name</h3>
              </div>
              <div class="panel-body">
                <small>Published: <time datetime="">datetime</time></small>
                <p>Cost: <b>5</b></p>
                <p>Address: <b>Lorem ipsum dolor.</b></p>
                <p>Comment: </p>
                <blockquote>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt cupiditate modi odit temporibus qui, soluta omnis in, porro recusandae corrupti.</p>
                </blockquote>
                <a href="#" class="btn btn-primary btn-block">Select and send message</a>
              </div>
            </div>
          </div>
          <nav>
            <ul class="pagination">
              <li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>