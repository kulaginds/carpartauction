<?php

/* @var $this yii\web\View */

$this->title = 'Создать заявку';
$this->registerJsFile('js/jquery.validate.min.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/jquery.validate.additional.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
$this->registerJsFile('js/create.js', ['depends' => ['yii\bootstrap\BootstrapAsset']]);
?>
          <h1><?= $this->title ?></h1>
          <form class="form-horizontal" id="claim_form" method="post">
            <div class="form-group">
              <label for="inputType1" class="col-sm-2 control-label">Car model</label>
              <div class="col-sm-10">
                <select name="model" id="inputType1" class="form-control">
                  <option value="1">Mazda</option>
                  <option value="2">Chevrolet</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="inputName2" class="col-sm-2 control-label">Part name</label>
              <div class="col-sm-10">
                <input name="name" type="text" class="form-control" id="inputName2" placeholder="Part name">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Create</button>
              </div>
            </div>
          </form>