<?php

namespace app\components;

use yii\base\Widget;

class AuthWidget extends Widget
{
	public function init()
	{
		parent::init();

		//
	}

	public function run()
	{
		return '<div class="panel panel-success">
                <div class="panel-heading">
                  <h3 class="panel-title">Authorization</h3>
                </div>
                <div class="panel-body">
                  <p>Hello, $USER! <a href="#" class="btn btn-sm btn-primary">Logout</a></p>
                  <p>Sections:</p>
                  <div class="list-group">
                    <a href="/index.php?r=site/index" class="list-group-item">Новости</a>
                    <a href="/index.php?r=admin/index" class="list-group-item">Админка</a>
                    <a href="/index.php?r=claims/my" class="list-group-item">Мои заявки</a>
                    <a href="/index.php?r=claims/index" class="list-group-item">Заявки</a>
                    <a href="/index.php?r=messages/index" class="list-group-item">Сообщения</a>
                  </div>
                  <!-- <form class="form-signin">
                    <p class="form-signin-heading">Please sign in</p>
                    <label for="inputEmail" class="sr-only">Email address</label>
                    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                    <label for="inputPassword" class="sr-only">Password</label>
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <a href="#" class="btn btn-lg btn-primary btn-block">Register</a>
                  </form> -->
                </div>
              </div>';
	}
}