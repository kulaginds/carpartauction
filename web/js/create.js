$(document).ready(function () {
	$("#claim_form").validate({
		rules: {
			name: {
				required: true,
				minlength: 4,
				maxlength: 255
			}
		}
	});
});