$(document).ready(function () {
	$("#proposition_form").validate({
		rules: {
			cost: {
				required: true,
				digits: true,
				min: 1
			},
			address: {
				required: true
			},
			comment: {
				required: false,
				maxlength: 255
			}
		}
	});
});