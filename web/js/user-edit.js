$(document).ready(function () {

  $("#user_edit_form").validate({
    rules: {
      name: {
        required: true,
        minlength: 4,
        maxlength: 40
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 8,
        maxlength: 16,
        one_digit: true, 
        one_latin_letter: true
      },
      type: {
        required: true
      }
    },
    errorClass: "text-danger",
    pendingClass: "text-info",
    validClass: "text-success"
  });

  var genpass = function () {
    return Math.random().toString(36).slice(-8);
  }

  window.generateTo = function (to_selector) {
    $(to_selector).val(genpass());
    return false;
  };

});