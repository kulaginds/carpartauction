$(document).ready(function () {

  $("#register_form").validate({
    rules: {
      name: {
        required: true,
        minlength: 4,
        maxlength: 40
      },
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 8,
        maxlength: 16,
        one_digit: true, 
        one_latin_letter: true
      },
      password2: {
        required: true,
        equalTo: '#register_form input[name=password]'
      },
      type: {
        required: true
      }
    },
    errorClass: "text-danger",
    pendingClass: "text-info",
    validClass: "text-success"
  });

});