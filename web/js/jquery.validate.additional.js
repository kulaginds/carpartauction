$(document).ready(function () {
  $.validator.addMethod('one_digit', function (value, el) {
    return /[0-9]/.test(value);
  }, 'Пожалуйста, используйте хотя-бы одну цифру.');
  $.validator.addMethod('one_latin_letter', function (value, el) {
    return /[a-z]/i.test(value);
  }, 'Пожалуйста, используйте хотя-бы одну латинскую букву');

  $.validator.messages['required'] = "Это обязательное поле.";
  $.validator.messages['minlength'] = $.validator.format( "Пожалуйста, используйте не менее {0} символов." );
  $.validator.messages['maxlength'] = $.validator.format( "Пожалуйста, используйте не более {0} символов." );
  $.validator.messages['email'] = "Пожалуйста, введите корректный e-mail.";
  $.validator.messages['equalTo'] = "Пожалуйста, введите то же значение снова.";
  $.validator.messages['min'] = $.validator.format( "Пожалуйста, введие значение больше либо равное {0}." );
});